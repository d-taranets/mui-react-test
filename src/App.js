import DefaultLayout from "./components/layouts/DefaultLayout/DefaultLayout";
import Calculator from "./components/common/Calculator/Calculator";

function App() {
    return (
        <div className="App">
            <DefaultLayout>
                <Calculator/>
            </DefaultLayout>
        </div>
    );
}

export default App;
