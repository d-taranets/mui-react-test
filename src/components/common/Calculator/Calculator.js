import {CalculatorStyles} from "./CalculatorStyles";
import {DefaultLayoutContext} from "../../../contexts/DefaultLayoutContext";
import {TextField} from "@mui/material";
import {useContext, useEffect, useMemo, useState} from "react";

const Calculator = () => {
    const blockPrice = 10;
    const {rate} = useContext(DefaultLayoutContext);
    const [amount, setAmount] = useState(0);

    const price = useMemo(() => {
        return (amount * blockPrice * rate).toFixed(2)
    }, [amount, rate])

    useEffect(() => {
        if (amount < 0) {
            setAmount(0)
        }
    }, [amount])


    return (
        <CalculatorStyles>
            <TextField
                type={'number'}
                InputProps={{inputProps: {min: 0}}}
                label="Amount"
                variant="outlined"
                value={amount}
                onChange={e => setAmount(e.target.value)}
            />
            <TextField
                type={'number'}
                label="Price"
                variant="outlined"
                value={price}
                disabled
            />
        </CalculatorStyles>
    )
}

export default Calculator