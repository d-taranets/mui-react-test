import {styled} from "@mui/material";

export const CalculatorStyles = styled('div')`
  padding: 20px;
  display: flex;

  .MuiFormControl-root {
    margin: 0 10px;
  }
`;