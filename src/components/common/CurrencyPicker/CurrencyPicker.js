import SelectInput from "../../inputs/SelectInput/SelectInput";
import {DefaultLayoutContext} from "../../../contexts/DefaultLayoutContext";
import {useContext} from "react";
import theme from "../../../config/theme";
import {currencyItems} from "./currency.config";


const inputProps = {
    MenuProps: {
        MenuListProps: {
            sx: {
                backgroundColor: theme.palette.primary.background,
                minWidth: "110px",
            }
        }
    }
}

const CurrencyPicker = () => {
    const {rate, setRate} = useContext(DefaultLayoutContext);
    return (
        <SelectInput
            inputProps={inputProps}
            name={'currency-select'}
            items={currencyItems}
            value={rate}
            handleChange={(e, item) => setRate(item.props.value)}
        />
    )
}

export default CurrencyPicker