import {Accordion, AccordionDetails, AccordionSummary} from "@mui/material";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {currencyItems} from "./currency.config";
import {DefaultLayoutContext} from "../../../contexts/DefaultLayoutContext";
import {useContext} from "react";

export const CurrencyItem = ({item, active}) => {
    const {setRate} = useContext(DefaultLayoutContext);
    const {label, icon, value} = item;

    return (
        <div className={`currency-item ${active ? 'active-item' : ''}`} key={label} onClick={() => setRate(value)}>
            {icon && <img src={icon} alt={label}/>}
            <div>
                {label}
            </div>
        </div>
    )
}


const CurrencyPickerAccordion = () => {
    const {rate} = useContext(DefaultLayoutContext);
    const activeItem = currencyItems.find(({value}) => value === rate);

    return (
        <Accordion className={'currency-accordion'}>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon/>}
                aria-controls="panel2a-content"
                id="panel2a-header"
            >
                <div>
                    <CurrencyItem item={activeItem} active={false}/>
                </div>
            </AccordionSummary>
            <AccordionDetails>
                <div>
                    {currencyItems.map(item => (
                        <CurrencyItem item={item}
                                      active={activeItem.value === item.value}
                                      key={item.value}
                        />
                    ))}
                </div>
            </AccordionDetails>
        </Accordion>
    )
}

export default CurrencyPickerAccordion