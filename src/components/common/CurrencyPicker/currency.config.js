import usdIcon from "./cur/usd.png";
import eurIcon from "./cur/eur.png";
import gbpIcon from "./cur/gbp.png";
import audIcon from "./cur/aud.png";
import cadIcon from "./cur/cad.png";

export const currencyItems = [
    {
        label: 'usd',
        value: 1,
        icon: usdIcon
    }, {
        label: 'eur',
        value: 0.93,
        icon: eurIcon
    }, {
        label: 'gbp',
        value: 0.81,
        icon: gbpIcon
    }, {
        label: 'aud',
        value: 1.52,
        icon: audIcon
    }, {
        label: 'cad',
        value: 1.36,
        icon: cadIcon

    }
]