import {FormControl} from "@mui/material";
import {MenuItemStyled, SelectStyled} from "./SelectInputStyles";
import {KeyboardArrowDown, KeyboardArrowUp} from "@mui/icons-material";
import {useState} from "react";

const SelectInput = props => {
    const {items, value, handleChange, name, inputProps} = {inputProps: {}, ...props};
    const [isOpen, setIsOpen] = useState(false);


    return (
        <FormControl fullWidth>
            <SelectStyled
                className={'select-input'}
                id={name}
                onOpen={() => setIsOpen(true)}
                onClose={() => setIsOpen(false)}
                value={value}
                label={name}
                onChange={handleChange}
                inputProps={inputProps}
                IconComponent={() => (
                    isOpen ? <KeyboardArrowUp className={'open'}/> : <KeyboardArrowDown/>
                )}
            >
                {
                    items.map(({value, label, icon}) => (
                        <MenuItemStyled value={value} key={value}>
                            {icon && <img src={icon} alt={label}/>}
                            <div>
                                {label}
                            </div>
                        </MenuItemStyled>
                    ))
                }
            </SelectStyled>
        </FormControl>
    )
}

export default SelectInput