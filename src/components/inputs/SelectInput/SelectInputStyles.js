import {MenuItem, Select, styled} from "@mui/material";

export const MenuItemStyled = styled(MenuItem)`
  display: flex;
  text-transform: uppercase;
  font-size: 14px;
  color: ${({theme}) => theme.palette.primary.textLight};

  img {
    width: 20px;
    height: 20px;
    padding-right: 8px;
  }

  &.Mui-selected {
    background: inherit;
    color: ${({theme}) => theme.palette.secondary.main};
    border-right: 2px solid ${({theme}) => theme.palette.secondary.main};;

    &:hover {
      background: inherit;
      color: ${({theme}) => theme.palette.secondary.main};
      border-right: 2px solid ${({theme}) => theme.palette.secondary.main};;
    }
  }

  &:hover {
    background: inherit;
    color: ${({theme}) => theme.palette.secondary.main};
    border-right: 2px solid ${({theme}) => theme.palette.secondary.main};;
  }
`

export const SelectStyled = styled(Select)`
  .MuiSelect-select {
    display: flex;
    align-items: center;
    text-transform: uppercase;
    font-size: 14px;
    color: ${({theme}) => theme.palette.primary.textLight};
  }

  .MuiOutlinedInput-notchedOutline {
    border: none;
  }

  img {
    width: 16px;
    height: 16px;
    padding-right: 8px;
  }

  & .MuiSvgIcon-root {
    font-size: 20px;
    pointer-events: none;
    position: absolute;
    right: 6px;
    cursor: pointer;
    color: ${({theme}) => theme.palette.primary.textLight};

    &.open {
      cursor: pointer;
      color: ${({theme}) => theme.palette.secondary.main};
    }
  }

,
`