import DefaultLayoutHeader from "./DefaultLayoutHeader/DefaultLayoutHeader";
import {DefaultLayoutContext} from "../../../contexts/DefaultLayoutContext";
import {useState} from "react";

const DefaultLayout = props => {
    const [rate, setRate] = useState(1);
    const [tabIndex, setTabIndex] = useState(0);

    return (
        <DefaultLayoutContext.Provider value={{
            rate,
            setRate,
            tabIndex,
            setTabIndex
        }}>
            <DefaultLayoutHeader/>
            {props.children}
        </DefaultLayoutContext.Provider>
    )
}

export default DefaultLayout