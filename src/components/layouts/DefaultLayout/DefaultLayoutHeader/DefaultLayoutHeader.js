import * as React from 'react';
import {useContext, useState} from 'react';
import {HeaderAppBar, HeaderContainer, HeaderTab, HeaderTabs} from "./DefaultlayoutHeaderStyles";
import CurrencyPicker from "../../../common/CurrencyPicker/CurrencyPicker";
import {Button, Drawer} from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import {DefaultLayoutContext} from "../../../../contexts/DefaultLayoutContext";
import DrawerContent from "../DrawerContent/DrawerContent";

const pages = ['OSRS Gold', 'RS3 Gold', 'Sell RS Gold', 'OSRS Items', 'OSRS Accounts', 'Reward Chests'];

function DefaultLayoutHeader() {
    const {tabIndex, setTabIndex} = useContext(DefaultLayoutContext);
    const [openBurger, setOpenBurger] = useState(false)

    return (
        <HeaderAppBar position="static">
            <HeaderContainer maxWidth="xl">
                <div className={'mobile-only'}>
                    <React.Fragment key={'left'}>
                        <Button disableRipple onClick={() => setOpenBurger(true)}>
                            <MenuIcon/>
                        </Button>
                        <Drawer
                            anchor={'left'}
                            open={openBurger}
                            onClose={() => {
                                setOpenBurger(false)
                            }}
                        >
                            <DrawerContent
                                closeDrawer={() => setOpenBurger(false)}
                                items={pages}
                            />
                        </Drawer>
                    </React.Fragment>
                </div>
                <a href="/">
                    <img src="/assets/problemas.png" alt="image"/>
                </a>
                <div className={'desktop-only'}>
                    <HeaderTabs
                        variant={'fullWidth'}
                        value={tabIndex}
                        onChange={(e, value) => setTabIndex(value)}
                    >
                        {pages.map(page =>
                            <HeaderTab
                                disableRipple
                                label={page}
                                key={page}
                            />)
                        }
                    </HeaderTabs>
                    <CurrencyPicker/>
                    <Button variant="text">Sign Up</Button>
                </div>
                <Button variant="contained">Log In</Button>
            </HeaderContainer>
        </HeaderAppBar>
    );
}

export default DefaultLayoutHeader;
