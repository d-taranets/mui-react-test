import {Container, styled, Tab, Tabs} from "@mui/material";
import AppBar from "@mui/material/AppBar";

export const HeaderAppBar = styled(AppBar)`
  background: ${({theme}) => theme.palette.primary.background};

  .desktop-only {
    display: contents;
  }

  .mobile-only {
    display: none;
  }

  @media (max-width: 1250px) {
    .desktop-only {
      display: none;
    }

    .mobile-only {
      display: contents;
    }
  }
`

export const HeaderContainer = styled(Container)`
  height: 80px;
  display: flex;
  align-items: center;

  .select-input {
    margin-right: 5px;
    margin-left: auto;
  }

  &.MuiContainer-root {
    padding: 0 60px 0 70px;
  }

  .MuiButtonBase-root {
    white-space: nowrap;
    min-width: 90px;
    max-height: 34px;
    text-transform: none;
  }

  .MuiButton-text {
    color: ${({theme}) => theme.palette.primary.textLight};

    &:hover {
      background: inherit;
      color: ${({theme}) => theme.palette.secondary.main};
    }
  }

  @media (max-width: 1315px) {
    justify-content: space-between;
  }

  @media (max-width: 600px) {
    &.MuiContainer-root {
      padding: 0 16px 0 16px;
    }

    .MuiButtonBase-root {
      max-width: 74px;
      min-width: auto;
    }

    .mobile-only {
      .MuiButtonBase-root {
        padding: 0;

        svg {
          font-size: 31px;
        }
      }
    }

  }
`

export const HeaderTabs = styled(Tabs)`
  margin-left: 24px;
  overflow: visible;

  .MuiTabs-indicator {
    max-width: 24px;
    margin: 0 18px;
    bottom: 10px;
    height: 1px;
  }
`

export const HeaderTab = styled(Tab)`
  text-transform: none;
  padding: 2px 15px;
  color: ${({theme}) => theme.palette.primary.textLight};
  white-space: nowrap;
  max-width: max-content;
`