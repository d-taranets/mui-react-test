import {DrawerContainer} from "./DrawerContentStyles";
import {Button} from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';
import {DefaultLayoutContext} from "../../../../contexts/DefaultLayoutContext";
import {useContext} from "react";
import CurrencyPickerAccordion from "../../../common/CurrencyPicker/CurrencyPickerAccordion";

const DrawerContent = props => {
    const {closeDrawer, items} = props;
    const {tabIndex, setTabIndex} = useContext(DefaultLayoutContext);

    return (
        <DrawerContainer>
            <div className={'close-block'}>
                <Button onClick={closeDrawer} disableRipple>
                    <CloseIcon/>
                </Button>
            </div>
            <div className={'separator'}/>
            {items.map((item, key) => (
                <div className={'page-block'} key={key}>
                    <div className={`page-item ${tabIndex === key ? 'active-page' : ''}`}
                         onClick={() => setTabIndex(key)}>
                        {item}
                    </div>
                    <div className={'separator'}/>

                </div>
            ))}
            <CurrencyPickerAccordion/>
            <div className={'separator'}/>
            <div className={'buttons-block'}>
                <Button variant="outlined">Sign Up</Button>
                <Button variant="contained">Log In</Button>
            </div>
        </DrawerContainer>
    )
}

export default DrawerContent