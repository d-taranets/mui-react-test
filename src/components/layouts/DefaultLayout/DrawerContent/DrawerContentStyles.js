import {styled} from "@mui/material";

export const DrawerContainer = styled('div')`
  display: flex;
  flex-direction: column;
  background: ${({theme}) => theme.palette.primary.background};
  height: 100%;
  width: 240px;
  position: relative;
  padding: 0 15px;

  .close-block {
    height: 78px;
    display: flex;
    align-items: center;

    button {
      min-width: 38px;

      &:hover {
        background: inherit;

        svg {
          color: ${({theme}) => theme.palette.secondary.main};
        }
      }
    }

    svg {
      font-size: 30px;
      color: ${({theme}) => theme.palette.primary.textLight};
    }
  }

  .separator {
    height: 1px;
    background: ${({theme}) => theme.palette.primary.textLight};
    opacity: 0.2;
  }

  .page-item {
    height: 43px;
    display: flex;
    align-items: center;
    color: ${({theme}) => theme.palette.primary.textLight};
    cursor: pointer;

    &.active-page, &:hover {
      color: ${({theme}) => theme.palette.secondary.main};
    }
  }

  .MuiButtonBase-root.Mui-expanded {
    margin: 0;
    min-height: 48px;
  }

  .MuiAccordionSummary-content.Mui-expanded, .currency-accordion.Mui-expanded {
    margin: 0;
  }

  .currency-accordion {
    background: ${({theme}) => theme.palette.primary.background};
    color: ${({theme}) => theme.palette.primary.textLight};
    box-shadow: none;
    margin: 0;

    .MuiAccordionSummary-content {
      margin: 0;
    }

    .MuiAccordionDetails-root {
      margin: 0;
      padding: 0;
    }

    svg {
      color: ${({theme}) => theme.palette.primary.textLight};
    }

    .MuiButtonBase-root {
      padding: 0;
      min-height: 38px;
    }

    .currency-item {
      display: flex;
      align-items: center;
      text-transform: uppercase;
      cursor: pointer;
      min-height: 38px;

      &.active-item {
        color: ${({theme}) => theme.palette.secondary.main};
      }

      img {
        max-width: 16px;
        max-height: 16px;
        margin-right: 10px;
      }
    }
  }

  .buttons-block {
    display: flex;
    padding: 15px 0;

    button {
      width: 80px;
      height: 34px;
      margin-right: 15px;
      text-transform: none;
      white-space: nowrap;
    }

    .MuiButton-outlined {
      color: ${({theme}) => theme.palette.primary.textLight};
      border-color: ${({theme}) => theme.palette.primary.textLight};
    }
  }

`;