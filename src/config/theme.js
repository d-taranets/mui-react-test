import {createTheme, responsiveFontSizes} from '@mui/material/styles'

const theme = createTheme({
    typography: {
        fontFamily: 'Poppins',
    },
    palette: {
        primary: {
            main: '#E9B109',
            background: '#142241',
            textLight: '#FAFAFA'
        },
        secondary: {
            main: '#E9B109'
        },
        error: {
            main: '#19857b'
        }
    },
    props: {
        MuiButtonBase: {
            disableRipple: true,
        },
    }
})

export default responsiveFontSizes(theme)
