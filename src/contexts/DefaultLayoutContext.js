import {createContext} from "react";

export const DefaultLayoutContext = createContext({
    tabIndex: 0,
    setTabIndex: () => {
        return null;
    },
    rate: 1,
    setRate: () => {
        return null;
    },
});